<?php

class Disc extends Product{

   
    private $size;

    public function __construct($id,$sku,$name,$price,$size)
    {
        $this->setId($id);
        $this->setSku($sku);
        $this->setName($name);
        $this->setPrice($price);
        $this->setSize($size);
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($newsize)
    {
        $this->size = $newsize;
    }

}
<?php

class Furniture extends Product{

    private $width;
    private $heigth;
    private $length;

    public function __construct($id,$sku,$name,$price,$width,$heigth,$length)
    {
        $this->setId($id);
        $this->setSku($sku);
        $this->setName($name);
        $this->setPrice($price);
        $this->setWidth($width);
        $this->setLength($length);
        $this->setHeigth($heigth);
    }

    public function setWidth($newwidth)
    {
        $this->width = $newwidth;
    }
    
    public function getWidth()
    {
        return $this->width;
    }

    public function setHeigth($newheigth)
    {
        $this->heigth = $newheigth;
    }
    
    public function getHeigth()
    {
        return $this->heigth;
    }

    public function setLength($newlength)
    {
        $this->length = $newlength;
    }
    
    public function getLength()
    {
        return $this->length;
    }

}
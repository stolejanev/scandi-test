<?php

class Book extends Product{

    private $weigth;

    public function __construct($id,$sku,$name,$price,$weigth)
    {
        $this->setId($id);
        $this->setSku($sku);
        $this->setName($name);
        $this->setPrice($price);
        $this->setWeigth($weigth);
    }

    public function setWeigth($newweigth)
    {
        $this->weigth = $newweigth;
    }

    public function getWeigth()
    {
        return $this->weigth;
    }

}
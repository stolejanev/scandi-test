<?php

class Product {
    
    protected $id;
    protected $sku;
    protected $name;
    protected $price;
    
    public function getSku()
    {
        return $this->sku;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice()
    {
        return $this->price;
    }
    public function setSku($newsku)
    {
        $this->sku = $newsku;
    }

    public function setName($newname)
    {
        $this->name = $newname;
    }

    public function setPrice($newprice)
    {
        $this->price = $newprice;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
}
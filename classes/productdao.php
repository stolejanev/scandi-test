<?php

class ProductDao extends Dbc{

    
    public  function findAll()
    {

        $conn = $this->connect();
        if(is_numeric($conn))
        {
            return 1;
        }
        $stmt = $conn->prepare("SELECT * FROM product");
        $stmt->execute();
        $result = $stmt->get_result();
        if($result)
        {
        $numRows = $result->num_rows;
        if($numRows > 0)
        {
            while($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
             $stmt->close();
            return $data;
        }
        else {
            $stmt->close();
            return 2;
        }
        } else {
            $stmt->close();
            return 3;
        }
        
        
    }

    //Checks for UNIQUENESS of SKU for product
    public function checkSku($sku)
    {
        $conn = $this->connect();
        $stmt = $conn->prepare("SELECT COUNT(id) FROM product WHERE sku = ?");
        $stmt->bind_param("s",$sku);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result)
        {
        $numRows = $result->num_rows;
        if($numRows > 0)
        {
            while($row = $result->fetch_assoc())
            {
                $data[] = $row;
            }
             $conn->close();
            return $data[0]['COUNT(id)'];   
        }
        }
        return 0;
    }

    public function deleteProduct($id)
    {
        $conn = $this->connect();
        $stmt = $conn->prepare("DELETE FROM product WHERE id = ?");
        $stmt->bind_param("i",$id);
        $stmt->execute();
        $result = $stmt->get_result();
            if($result)
            {
                $conn->close();
                return 1;
            }
            
            else {
                $this->close;
                return 0;
            }
    }

    //Function to create a new product in table Product in DB
    public function saveProduct($sku,$name,$price,$type,$weigth=NULL,$width=NULL,$heigth=NULL,$length=NULL,$size=NULL)
    {
        $conn = $this->connect();
        $stmt = $conn->prepare("INSERT INTO `product` (`sku`, `name`, `price`, `type`, `weigth`,  `width`, `length`, `height`, `size`) 
        VALUES (?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param("ssdsiiiii",$sku,$name,$price,$type,$weigth,$width,$length,$heigth,$size);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result)
        {
            $conn->close();
            return 1;
        }
        
        else {
            $this->close;
            return 0;
        }
    }

    //Helper Constructor function for the OOP Product classes
    public function createProduct($product)
    {
        switch($product['type'])
        {
            case("B") : {
                $book = new Book($product['id'],$product['sku'],$product['name'],$product['price'],$product['weigth']);
                return $book;
            }
            case("F") : {
                $furniture = new Furniture($product['id'],$product['sku'],$product['name'],$product['price'],$product['width'],$product['height'],$product['length']);
                return $furniture;
            }
            case("D") : {
                $disc = new Disc($product['id'],$product['sku'],$product['name'],$product['price'],$product['size']);
                return $disc;
            }
            default : return 0;
           
        }
    }
}
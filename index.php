<?php
include_once $_SERVER['DOCUMENT_ROOT']."/classes/dbc.php";
include_once $_SERVER['DOCUMENT_ROOT']."/classes/productdao.php";
include_once $_SERVER['DOCUMENT_ROOT']."/models/product.php";
include_once $_SERVER['DOCUMENT_ROOT']."/models/book.php";
include_once $_SERVER['DOCUMENT_ROOT']."/models/disc.php";
include_once $_SERVER['DOCUMENT_ROOT']."/models/furniture.php";

$route = explode("?",$_SERVER['REQUEST_URI'])[0];

if($route == '/' or $route == '/index' or $route == '/index.php')
{
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && $_POST['id']) {
        foreach($_POST['id'] as $id)
        {
          $dao = new ProductDao();
          $dao->deleteProduct($id);
        }
      }
    include 'views/index-view.php';
} else if($route == '/add-product' or $route == '/add-product.php')
{
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $dao = new ProductDao();
        $result = $dao->saveProduct($_POST['sku'],$_POST['name'],$_POST['price'],$_POST['type'],$_POST['weigth'],$_POST['width'],$_POST['height'],$_POST['length'],$_POST['size']);
        header("Location: /index?result=$result");
        die();
      }
    include 'views/add-product.php';
} else {
    include 'views/404.php';
}
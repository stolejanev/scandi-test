<?php

include $_SERVER['DOCUMENT_ROOT']."/classes/dbc.php";
include $_SERVER['DOCUMENT_ROOT']."/classes/productdao.php";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $dao = new ProductDao();
    $data = $_POST['sku'];
    $result = $dao->checkSku($data);
    echo $result;
    exit;
}
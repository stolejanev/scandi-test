
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Add product to Scandi test</title>
  <meta name="description" content="Product Template">
  <meta name="author" content="Stole Janev">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>
<div class="container-fluid">
    <br>
    <br>
<div class="row">
<div class="col-md-8 col-sm-12">
          <h1 class="h1">Add Product</h1>
      </div>
      <div class="col-md-2 col-sm-6 text-center">
      <button id="savebtn" form="product_form"  type="button" class="btn btn-secondary btn-lg btn-center">Save</button>
      </div>
      <div class="col-md-2 col-sm-6 text-center">
      <button onclick="location.href='index'" type="button" class="btn btn-secondary btn-lg btn-center">CANCEL</button>
      </div>
</div>
<hr>
<div id="validate" class="alert alert-danger" role="alert">
  
</div>
<form id="product_form" action="/add-product" method="post">
 <div class="mb-3 row">
    <label for="sku" class="col-md-2 col-form-label">SKU:</label>
    <div class="col-md-4">
      <input name="sku" required type="text" class="form-control" id="sku" >
    </div>
  </div>
  <div class="mb-3 row">
    <label for="name" class="col-md-2 col-form-label">Name:</label>
    <div class="col-md-4">
      <input name="name" required type="text" class="form-control" id="name">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="price" class="col-md-2 col-form-label">Price:</label>
    <div class="col-md-4">
      <input name="price" required min="1" type="number" class="form-control" id="price">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="productType" class="col-md-2 col-form-label">Type Switcher:</label>
    <div class="col-md-4">
    <select name="type" required id="productType" class="form-select" aria-label="Default select example">
  <option id="DVD" value="D">DVD</option>
  <option id="Furniture" value="F">Furniture</option>
  <option id="Book" value="B">Book</option>
</select>
    </div>
  </div>
  <div id="dvdinput" class="mb-3 row">
  <p class="col-md-12">Please, provide size in MB</p>
    <label for="size" class="col-md-2 col-form-label">Size:</label>
    <div class="col-md-4">
      <input name="size" min="1" default="1" type="number" class="form-control" id="size">
    </div>
  </div>
  <div id="furninput">
    <div class="mb-3 row">
    <p class="col-md-12">Please, provide dimensions in HxWxL format</p>
    <label for="height" class="col-md-2 col-form-label">Height:</label>
    <div class="col-md-4">
      <input name="height" min="1" default="1" type="number" class="form-control" id="height">
    </div>
    </div>
    <div class="mb-3 row">
    <label for="width" class="col-md-2 col-form-label">Width:</label>
    <div class="col-md-4">
      <input name="width" min="1" default="1" type="number" class="form-control" id="width">
    </div>
    </div>
    <div class="mb-3 row">
    <label for="length" class="col-md-2 col-form-label">Length:</label>
    <div class="col-md-4">
      <input name="length" min="1" default="1" type="number" class="form-control" id="length">
    </div>
    </div>
    </div>
    <div id="bookinput" class="mb-3 row">
    <p class="col-md-12">Please, provide weight in KG</p>
    <label for="weight" class="col-md-2 col-form-label">Weight:</label>
    <div class="col-md-4">
      <input name="weight" min="1" default="1" type="number" class="form-control" id="weight">
    </div>
    </div>
</form>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
  //Initial type setup
  $(function() {
    $('#furninput').hide();
    $('#bookinput').hide();
    $('#validate').hide();
  });
  
  
  //Toggle product types
  $('#productType').change(function() {
      if($('#productType').val() == 'D') {
        $('#dvdinput').show();
        $('#furninput').hide();
        $('#bookinput').hide();
        $("#size").prop('required',true);
        $("#width").prop('required',false);
        $("#height").prop('required',false);
        $("#length").prop('required',false);
        $("#weight").prop('required',false);
      } else if($('#productType').val() == 'F')
      {
        $('#dvdinput').hide();
        $('#furninput').show();
        $('#bookinput').hide();
        $("#width").prop('required',true);
        $("#height").prop('required',true);
        $("#length").prop('required',true);
        $("#weight").prop('required',false);
        $("#size").prop('required',false);
      } else {
        $('#dvdinput').hide();
        $('#furninput').hide();
        $('#bookinput').show();
        $("#weight").prop('required',true);
        $("#width").prop('required',false);
        $("#height").prop('required',false);
        $("#length").prop('required',false);
        $("#size").prop('required',false);
      }
    });

    //Validating the form and checking for SKU unique
    $('#savebtn').click(function(){
      if($('#product_form')[0].checkValidity())
      {
        $.post("check-sku.php",{sku:$('#sku').val()},function(result){
          //$('#validate').text(result);
          //$('#validate').show();
          if(result == 0)
          {
          $('#product_form').submit();
          }
          else {
          $('#validate').text("Please, enter UNIQUE SKU");
          $('#validate').show();
          }
        })
       
      } else {
        $('#validate').text("Please, submit required data");
        $('#validate').show();
      }
    });

    </script>
</body>
</html>
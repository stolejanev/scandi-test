
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Scandi Product List</title>
  <meta name="description" content="Scandi Test">
  <meta name="author" content="Stole Janev">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>
<body>
<br>
<br>

<div class="container-fluid">
  <div class="row">
    <br>
    <br>
      <div class="col-md-8 col-sm-12">
          <h1 class="h1">Product List</h1>
      </div>
      <div class="col-md-2 col-sm-6 text-center">
      <button onclick="location.href='add-product'" type="button" class="btn btn-secondary btn-lg btn-center">ADD</button>
      </div>
      <div class="col-md-2 col-sm-6 text-center">
      <input id="#delete-product-btn" type="submit" form="delete" value="MASS DELETE" class="btn btn-secondary btn-lg btn-center"/>
      </div>
  </div>
  <hr>
  <form id="delete" method="post" action="/index">
  <div class="row">
     <?php
     $dao = new ProductDao();
     $datas = $dao->findAll();
     if(is_numeric($datas))
     {
         echo '<div class="col-12 text-center"><div class="h2">No Products</div></div>';
     } ?>
    <?php if(!is_numeric($datas)) foreach($datas as $data): ?>
      <?php $prod = $dao->createProduct($data) ?>
      <div class="col-sm-3 col-md-3">
        <div class="card">
        <input class="delete-checkbox" name="id[<?php echo $prod->getId(); ?>]" value="<?php echo $prod->getId(); ?>" type="checkbox" aria-label="Checkbox for deleting product">
        <div class="card-body text-center">';
        <h5 class="card-title"><?php echo $prod->getSku(); ?></h5>
        <p class="card-text"><b><?php echo $prod->getName(); ?></b></p>
        <p class="card-text"><?php echo $prod->getPrice(); ?></p>
        <?php if(function_exists('getWeigth')) : ?>
          <p class="card-text"><?php echo $prod->getWeigth(); ?></p>';
        <?php endif; ?>
        <?php if(function_exists('getHeigth')) : ?>
          <p class="card-text">Dimensions : <?php echo $prod->getHeigth(); ?> x <?php echo $prod->getWidth(); ?> x <?php echo $prod->getLength(); ?></p>
        <?php endif; ?>
        <?php if(function_exists('getSize')) : ?>
          <p class="card-text"><?php echo $prod->getSize(); ?></p>
        <?php endif; ?>
        </div>
      </div>
    </div>
    <?php endforeach; ?>
  </div>
  </form>
  <hr>
  <div class="row">
  <p class="col-12 text-center">ScandiWeb Test Assignment 2021</p>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>